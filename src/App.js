import Rutas from './rutas';
import AuthState from './context/auth/AuthState';
import ProductsState from './context/products/ProductsState';
import {GlobalStyle} from './style';

function App() {
  return (
    <AuthState>
      <ProductsState>
      <GlobalStyle />
        <Rutas />
      </ProductsState>
    </AuthState>
  );
}

export default App;

import { useState, useContext } from 'react';
import mensaje from './mensaje';
import AuthContext from '../../context/auth/AuthContext';

let msjEmailError = "";
let msjPswError = "";

const Login = () => {
  const [formValues, setFormValues] = useState({
    email: '',
    password: '',
  });

  const { iniciarSesion } = useContext(AuthContext);

  const handleChange = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = formValues;
    console.log("Datos de Formulario");
    console.log("email, password " + email + " " + password);
    if (validarFormulario(email, password)) {
      iniciarSesion(email, password);
    } else {
      mensaje("Error al iniciar session:\n" + msjEmailError + "\n" + msjPswError, 'warning');
    }
  };

  const validarFormulario = (email, password) => {
    let campoEmail = email;
    let campoPassword = password;
    let formularioValido = true;

    msjEmailError = "";
    msjPswError = "";

    // Email
    if (!campoEmail) {
      formularioValido = false;
      console.log("dentro de " + !campoEmail);
      msjEmailError = "Por favor, ingresa tu correo.";
    } else {
      // Validamos si el formato del Email es correcto 
      if (typeof campoEmail !== "undefined") {
        console.log("dentro de undefined");
        let posicionArroba = campoEmail.lastIndexOf('@');
        let posicionPunto = campoEmail.lastIndexOf('.');
        if (!(posicionArroba < posicionPunto && posicionArroba > 0 && campoEmail.indexOf('@@') == -1 && posicionPunto > 2 && (campoEmail.length - posicionPunto) > 2)) {
          formularioValido = false;
          msjEmailError = "Por favor, ingresa un correo válido.";
        }
      }
    }

    // Contraseña
    if (!campoPassword) {
      formularioValido = false;
      msjPswError = "Por favor, ingresa tu contraseña.";
    }

    return formularioValido;
  }

  return (
    <div className="container" id="log-in-form">
      <div className="heading">
        <h1>Iniciar Session</h1>
      </div>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <input
            name='email'
            type='email'
            className='form-control form-control-sm'
            onChange={handleChange}
            value={formValues.email}
            placeholder="Ingrese Email"
          />
        </div>
        <div className="form-group">
          <input
            type='password'
            className='form-control form-control-sm'
            name='password'
            onChange={handleChange}
            value={formValues.password}
            placeholder="Ingrese Contraseña"
          />
        </div>
        <div className="form-group form-group-btn">
          <button type='submit' className='btn btn-success'>
            Log In
          </button>
        </div>
      </form>
    </div>
  );
};

export default Login;
import React, { useContext, useEffect } from 'react';
import Tabla from '../../components/Tabla';
import { DivTabla } from './style';

import ProductsContext from '../../context/products/ProductsContext';

const Productos = () => {
  const { listaProductos, obtenerProductos, deleteOk } =
    useContext(ProductsContext);

  useEffect(() => {
    obtenerProductos();
  }, []);

  useEffect(() => {
    if (deleteOk) {
      obtenerProductos();
    }
  }, [deleteOk]);

  return (
    <div>
      <DivTabla>
        <h1>Productos</h1>
        {listaProductos.length > 0 && <Tabla listaProductos={listaProductos} />}
      </DivTabla>
    </div>
  );
};

export default Productos;

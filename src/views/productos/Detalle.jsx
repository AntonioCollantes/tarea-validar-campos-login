import React, { useContext, useEffect } from 'react';
import VerDetalle from '../../components/Formulario/VerDetalle';
import ProductsContext from '../../context/products/ProductsContext';

const DetalleProducto = (props) => {
  const { obtenerProducto, producto } = useContext(ProductsContext);

  useEffect(() => {
    obtenerProducto(props.match.params.id);
  }, []);

  const tieneProducto = Object.keys(producto).length;

  const volverListado = () => {
    props.history.push('/productos');
  }

  return (
    <div>
      {tieneProducto > 0 && (
        <VerDetalle
          producto={producto}
          volver={volverListado}
        />
      )}
    </div>
  );
};

export default DetalleProducto;
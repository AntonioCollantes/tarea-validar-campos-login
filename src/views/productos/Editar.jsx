import React, { useContext, useEffect } from 'react';
import Formulario from '../../components/Formulario';

import ProductsContext from '../../context/products/ProductsContext';

const EditarProducto = (props) => {
  const { obtenerProducto, producto, editarProducto, editOk } =
    useContext(ProductsContext);

  useEffect(() => {
    obtenerProducto(props.match.params.id);
  }, []);

  useEffect(() => {
    if (editOk) {
      props.history.push('/productos');
    }
  }, [editOk]);

  const tieneProducto = Object.keys(producto).length;

  return (
    <div>
      <h1>Editar Producto</h1>
      {tieneProducto > 0 && (
        <Formulario
          producto={producto}
          editar={true}
          editarProducto={editarProducto}
        />
      )}
    </div>
  );
};

export default EditarProducto;

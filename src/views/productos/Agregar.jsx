import React, { useContext, useEffect } from 'react';
import Formulario from '../../components/Formulario';

import ProductsContext from '../../context/products/ProductsContext';

const AgregarProducto = (props) => {
  const { addOk, agregarProducto } = useContext(ProductsContext);

  useEffect(() => {
    if (addOk) {
      // redirigir
      props.history.push('/productos');
    }
  }, [addOk]);

  const nuevoProducto = (producto) => {
    agregarProducto(producto);
  };

  return (
    <div>
      <h1 style={{ color: 'RED'}}>Agregar Producto</h1>
      <Formulario agregarProductoNuevo={nuevoProducto} />
    </div>
  );
};

export default AgregarProducto;

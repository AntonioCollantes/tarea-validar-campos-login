const reducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        email: action.payload.email,
        uid: action.payload.uid,
      };
    case 'LOGOUT':
      return {
        email: null,
        uid: null,
      };

    default:
      return state;
  }
};

export default reducer;

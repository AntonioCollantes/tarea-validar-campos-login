import React, { useReducer } from 'react';
import ProductsContext from './ProductsContext';
import ProductsReducer from './ProductsReducer';

import { db } from '../../firebase/firebaseConfig';

const ProductsState = ({ children }) => {
  const initialState = {
    listaProductos: [],
    producto: {},
    addOk: false,
    editOk: false,
    deleteOk: false,
  };

  const agregarProducto = async (producto) => {
    try {
      await db.collection('productos').add(producto);
      dispatch({
        type: 'AGREGAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const obtenerProductos = async () => {
    try {
      const productos = [];
      const info = await db.collection('productos').get();
      info.forEach((item) => {
        productos.push({
          id: item.id,
          ...item.data(),
        });
      });

      dispatch({
        type: 'LLENAR_PRODUCTOS',
        payload: productos,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const obtenerProducto = async (id) => {
    try {
      const info = await db.collection('productos').doc(id).get();
      let producto = {
        id: id,
        ...info.data(),
      };
      dispatch({
        type: 'OBTENER_PRODUCTO',
        payload: producto,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const editarProducto = async (producto) => {
    try {
      const productoUpdate = { ...producto };
      delete productoUpdate.id;

      await db.collection('productos').doc(producto.id).update(productoUpdate);
      dispatch({
        type: 'EDITAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const eliminarProducto = async (id) => {
    try {
      await db.collection('productos').doc(id).delete();
      dispatch({
        type: 'ELIMINAR_PRODUCTO',
        payload: true,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const [state, dispatch] = useReducer(ProductsReducer, initialState);

  return (
    <ProductsContext.Provider
      value={{
        addOk: state.addOk,
        editOk: state.editOk,
        deleteOk: state.deleteOk,
        listaProductos: state.listaProductos,
        producto: state.producto,
        agregarProducto,
        obtenerProductos,
        obtenerProducto,
        editarProducto,
        eliminarProducto,
      }}
    >
      {children}
    </ProductsContext.Provider>
  );
};

export default ProductsState;

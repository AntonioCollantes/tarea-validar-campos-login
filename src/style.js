import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
html{
    background-color: #3c3838;
}

body{
    background-color: transparent;
}

#log-in-form{
    margin: 100px auto 0;
    padding: 100px;
    color: white;
    height: 660px;
    max-width: 1000px;
    background-image: url("https://estaticos.muyinteresante.es/media/cache/1140x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-marron_0.jpg");
    background-size: cover;
}

#log-in-form .heading{
	text-align: center;
    margin-top: 50px;
    margin-bottom: 70px;
}

#log-in-form .form-group{
    float: left;
    width: 30%;
    margin-left: 20px;
}

#log-in-form .form-control{
    padding: 15px 25px;
    height: auto;
}

#log-in-form .form-group-btn{
    width: 25%;
    margin-left: 50px;
}

#log-in-form .btn{
    padding: 13px 32px;
}

@media screen and (max-width: 800px){
    #log-in-form{
        margin: 200px;
    }

    #log-in-form .form-group{
        width: 60%;
        float: none;
        margin: 10px auto;
    }

    #log-in-form .form-group-btn{
        margin-top: 30px;
    }

    #log-in-form .btn{
        width: 100%;
    }
}

@media screen and (max-width: 600px){
    #log-in-form .form-group,
}`;
import React, { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

import AuthContext from '../../context/auth/AuthContext';

const Header = () => {
  const { cerrarSesion } = useContext(AuthContext);

  const handleClick = (e) => {
    e.preventDefault();
    cerrarSesion();
  };

  return (
    <nav className='navbar navbar-expand-lg navbar-light bg-light'>
      <Link className='navbar-brand' to='/'>
        Comidas
      </Link>
      <div className='collapse navbar-collapse container-fluid'>
        <ul className='navbar-nav mr-auto mt-2 mt-lg-0'>
          <li className='nav-item'>
            <NavLink className='nav-link' to='/productos'>
              Productos
            </NavLink>
          </li>
          <li className='nav-item'>
            <NavLink className='nav-link' to='/producto/nuevo'>
              Agregar Productos
            </NavLink>
          </li>
        </ul>
        <div className='d-flex'>
          <button onClick={handleClick} className='mr btn btn-warning'>
            Cerrar Sesión
          </button>
        </div>
      </div>
    </nav>
  );
};

export default Header;

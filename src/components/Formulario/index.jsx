import React, { useState, useEffect } from 'react';

const initialDataForm = {
  nombre: '',
  precio: '',
  categoria: '',
  descripcion: '',
};

const errorInit = {
  ...initialDataForm,
};

const Formulario = ({
  agregarProductoNuevo,
  producto,
  editar = false,
  editarProducto,
}) => {
  const [dataForm, setDataForm] = useState(initialDataForm);
  const [errors, setError] = useState(errorInit);

  useEffect(() => {
    if (editar) {
      setDataForm({ ...producto });
    }
  }, []);

  const handleChange = (e) => {
    setDataForm({ ...dataForm, [e.target.name]: e.target.value });
    setError({ ...errors, [e.target.name]: '' });
  };

  const isValid = () => {
    let respuesta = true;

    const localErros = { ...errorInit };

    for (let key in dataForm) {
      if (key !== 'id') {
        if (dataForm[key].trim() === '' || dataForm[key].length === 0) {
          localErros[key] = 'Campo requerido';
          respuesta = false;
        }
      }
    }

    setError({ ...localErros });

    return respuesta;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isValid()) {
      if (!editar) {
        agregarProductoNuevo(dataForm);
      } else {
        // vamos a editar
        editarProducto(dataForm);
      }
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className='form-row row'>
        <div className='form-group col-md-6'>
          <label style={{ color: 'yellow'}} htmlFor='nombre'>Nombre</label>
          <input
            type='text'
            className='form-control'
            name='nombre'
            onChange={handleChange}
            value={dataForm.nombre}
          />
          <span style={{ color: 'red', fonSize: '14px' }}>{errors.nombre}</span>
        </div>
        <div className='form-group col-md-6'>
          <label style={{ color: 'yellow'}} htmlFor='precio'>Precio</label>
          <input
            type='text'
            className='form-control'
            name='precio'
            onChange={handleChange}
            value={dataForm.precio}
          />
          <span style={{ color: 'red', fonSize: '14px' }}>{errors.precio}</span>
        </div>
      </div>
      <div className='form-group d-flex justify-content-around mt-2'>
        <div className='form-check'>
          <input
            className='form-check-input'
            type='radio'
            name='categoria'
            value='Bebidas'
            onChange={handleChange}
            checked={dataForm.categoria === 'Bebidas'}
          />

          <label style={{ color: 'yellow'}} className='form-check-label' htmlFor='Bebidas'>
            Bebidas
          </label>
        </div>
        <div className='form-check'>
          <input
            className='form-check-input'
            type='radio'
            name='categoria'
            value='Cortes'
            onChange={handleChange}
            checked={dataForm.categoria === 'Cortes'}
          />
          <label style={{ color: 'yellow'}} className='form-check-label' htmlFor='Cortes'>
            Cortes
          </label>
        </div>
        <div className='form-check'>
          <input
            className='form-check-input'
            type='radio'
            name='categoria'
            value='Entradas'
            onChange={handleChange}
            checked={dataForm.categoria === 'Entradas'}
          />
          <label style={{ color: 'yellow'}} className='form-check-label' htmlFor='Entradas'>
            Entradas
          </label>
        </div>
        <div className='form-check'>
          <input
            className='form-check-input'
            type='radio'
            name='categoria'
            value='Postres'
            onChange={handleChange}
            checked={dataForm.categoria === 'Postres'}
          />
          <label style={{ color: 'yellow'}} className='form-check-label' htmlFor='Postres'>
            Postres
          </label>
        </div>

        <span style={{ color: 'red', fonSize: '14px' }}>
          {errors.categoria}
        </span>
      </div>
      <div className='text-center'></div>
      <div className='form-group'>
        <label style={{ color: 'yellow'}} htmlFor='descripcion'>Descripción</label>
        <textarea
          className='form-control'
          name='descripcion'
          id='exampleFormControlTextarea1'
          rows='3'
          onChange={handleChange}
          value={dataForm.descripcion}
        ></textarea>
        <span style={{ color: 'red', fonSize: '14px' }}>
          {errors.descripcion}
        </span>
      </div>
      <button type='submit' className='btn btn-primary mt-4'>
        {editar ? 'Editar Producto' : 'Agregar Producto'}
      </button>
    </form>
  );
};

export default Formulario;

import React, { useContext } from 'react';
import { Link } from 'react-router-dom';

import ProductsContext from '../../context/products/ProductsContext';

const RowTabla = ({ info, indice }) => {
  const { id, nombre, precio, categoria } = info;

  const { eliminarProducto } = useContext(ProductsContext);
  return (
    <tr>
      <th scope='row'>{indice + 1}</th>
      <td>{nombre}</td>
      <td>{categoria}</td>
      <td>S/ {precio}</td>
      <td className='d-flex justify-content-around'>
        <Link className='btn btn-warning' to={`/producto/editar/${id}`}>
          Editar
        </Link>
        <Link className='btn btn-warning' to={`/producto/detalle/${id}`}>
          Ver Detalle
        </Link>
        <button className='btn btn-danger' onClick={() => eliminarProducto(id)}>
          Eliminar
        </button>
      </td>
    </tr>
  );
};

export default RowTabla;

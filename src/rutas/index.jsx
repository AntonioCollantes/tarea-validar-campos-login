import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

import { firebase } from '../firebase/firebaseConfig';

import Login from '../views/Login';
import Productos from '../views/productos';
import Agregar from '../views/productos/Agregar';
import Editar from '../views/productos/Editar';
import Detalle from '../views/productos/Detalle';

import RutaPublica from './RutaPublica';
import RutaPrivada from './RutaPrivada';

const Rutas = () => {
  const [estasAutenticado, setEstasAutenticado] = useState(false);
  const [consultado, setConsultando] = useState(true);

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      console.log(user);
      if (user?.uid) {
        setEstasAutenticado(true);
      } else {
        setEstasAutenticado(false);
      }
      setConsultando(false);
    });
  }, []);

  if (consultado) return <h1 className='text-center'>Cargando .....</h1>;

  return (
    <Router>
      <Switch>
        <RutaPublica
          estasAutenticado={estasAutenticado}
          exact
          path='/'
          component={Login}
        />
        <RutaPrivada
          estasAutenticado={estasAutenticado}
          exact
          path='/productos'
          component={Productos}
        />
        <RutaPrivada
          estasAutenticado={estasAutenticado}
          exact
          path='/producto/nuevo'
          component={Agregar}
        />
        <RutaPrivada
          estasAutenticado={estasAutenticado}
          exact
          path='/producto/editar/:id'
          component={Editar}
        />
        <RutaPrivada
          estasAutenticado={estasAutenticado}
          exact
          path='/producto/detalle/:id'
          component={Detalle}
        />
      </Switch>
    </Router>
  );
};

export default Rutas;
